# PrOwOvider

A lightweight identity provider

## Features

* Multi-Organisation shared deployment
* User 2FA using TOTP
* Login page uses zero Javascript (this will become optional in the future when FIDO2 2FA support is added)
* OpenID Connect provider
    * Superset of OAuth2 (you can use PrOwOvider as an OAuth2 provider too)
    * Authorization Code flow
    * Refresh Tokens
    * Full discovery support (including JWKs)
    * Token introspection
    * Custom claims with full control over what tokens they end up in and what users have them


## Screenshot

![Login](./static/screenshot-login.png)
