FROM rust:1.78

EXPOSE 80

WORKDIR /usr/src/prowovider
COPY . .

RUN SQLX_OFFLINE=true cargo install --path ./prowovider-core
CMD ["prowovider-core", "/settings.yaml"]