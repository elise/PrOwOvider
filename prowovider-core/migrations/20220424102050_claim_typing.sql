CREATE TYPE claim_type AS ENUM ('bool', 'number', 'string', 'array', 'object');

ALTER TABLE organisation_claims ADD COLUMN type claim_type;
