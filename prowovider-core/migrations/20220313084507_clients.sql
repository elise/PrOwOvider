CREATE TYPE client_type AS ENUM ('confidential', 'public');

CREATE TABLE organisation_clients(
    id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    name TEXT NOT NULL,
    client_type client_type NOT NULL,
    organisation_id uuid NOT NULL,
    secret TEXT NOT NULL,

    CONSTRAINT fk_organisation_id
      FOREIGN KEY(organisation_id)
        REFERENCES organisations(id)
          ON UPDATE CASCADE
            ON DELETE CASCADE
);

CREATE TABLE client_redirect_uris(
  id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,

  client_id uuid NOT NULL,
  redirect_uri TEXT NOT NULL,
  
  CONSTRAINT fk_client_id
      FOREIGN KEY(client_id)
        REFERENCES organisation_clients(id)
          ON UPDATE CASCADE
            ON DELETE CASCADE,
  UNIQUE (client_id, redirect_uri)
)