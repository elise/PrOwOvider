ALTER TABLE organisation_scopes ADD COLUMN allowed_by_default BOOLEAN NOT NULL DEFAULT FALSE;
UPDATE organisation_scopes SET allowed_by_default=TRUE WHERE scope='openid';
