CREATE TYPE claim_destination AS ENUM ('id_token', 'access_token', 'user_info');

CREATE TABLE organisation_claim_destinations(
    id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    
    organisation_id uuid NOT NULL,
    claim_name TEXT NOT NULL,
    destination claim_destination NOT NULL,

    CONSTRAINT fk_organisation_id
		FOREIGN KEY(organisation_id)
			REFERENCES organisations(id)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    CONSTRAINT fk_claim_name
		FOREIGN KEY(organisation_id, claim_name)
			REFERENCES organisation_claims(organisation_id, claim_name)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    UNIQUE (organisation_id, claim_name, destination)
);

INSERT INTO organisation_claim_destinations (organisation_id, claim_name, destination) (SELECT organisation_id, claim_name, 'id_token' FROM organisation_claims);
INSERT INTO organisation_claim_destinations (organisation_id, claim_name, destination) (SELECT organisation_id, claim_name, 'user_info' FROM organisation_claims);

CREATE TABLE organisation_scope_claim_map(
    id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,

    organisation_id uuid NOT NULL,
    scope TEXT NOT NULL,
    claim_name TEXT NOT NULL,

    CONSTRAINT fk_organisation_id
		FOREIGN KEY(organisation_id)
			REFERENCES organisations(id)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    CONSTRAINT fk_claim_name
		FOREIGN KEY(organisation_id, claim_name)
			REFERENCES organisation_claims(organisation_id, claim_name)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    UNIQUE (organisation_id, claim_name, scope)
);
