CREATE EXTENSION pgcrypto;

CREATE TABLE organisations(
    id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    name TEXT NOT NULL,
    UNIQUE (name)
);

CREATE TABLE organisation_users(
    id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    username TEXT NOT NULL,
    organisation_id uuid NOT NULL,
    bcrypt_password TEXT NOT NULL,

    CONSTRAINT fk_organisation_id
		FOREIGN KEY(organisation_id)
			REFERENCES organisations(id)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    UNIQUE (id, organisation_id),
    UNIQUE (username, organisation_id)
);

CREATE TABLE organisation_scopes(
    id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    organisation_id uuid NOT NULL,
    scope TEXT NOT NULL,
    CONSTRAINT fk_organisation_id
		FOREIGN KEY(organisation_id)
			REFERENCES organisations(id)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    UNIQUE (organisation_id, scope)
);

CREATE TABLE organisation_user_scopes(
    id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,

    organisation_id uuid NOT NULL,
    user_id uuid NOT NULL,
    scope TEXT NOT NULL,

    CONSTRAINT fk_organisation_id
		FOREIGN KEY(organisation_id)
			REFERENCES organisations(id)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    CONSTRAINT fk_user
        FOREIGN KEY(user_id, organisation_id)
            REFERENCES organisation_users(id, organisation_id)
                ON UPDATE CASCADE
                    ON DELETE CASCADE,
    CONSTRAINT fk_scope
		FOREIGN KEY(organisation_id, scope)
			REFERENCES organisation_scopes(organisation_id, scope)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    UNIQUE (organisation_id, user_id, scope)
);

CREATE TABLE organisation_claims(
    id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    organisation_id uuid NOT NULL,
    claim_name TEXT NOT NULL,
    CONSTRAINT fk_organisation_id
		FOREIGN KEY(organisation_id)
			REFERENCES organisations(id)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    UNIQUE (organisation_id, claim_name)
);

CREATE TABLE organisation_claim_entries(
    id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,

    organisation_id uuid NOT NULL,
    user_id uuid NOT NULL,
    claim_name TEXT NOT NULL,
    claim_value json NOT NULL,

    CONSTRAINT fk_organisation_id
		FOREIGN KEY(organisation_id)
			REFERENCES organisations(id)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    CONSTRAINT fk_user
        FOREIGN KEY(user_id, organisation_id)
            REFERENCES organisation_users(id, organisation_id)
                ON UPDATE CASCADE
                    ON DELETE CASCADE,
    CONSTRAINT fk_claim
		FOREIGN KEY(organisation_id, claim_name)
			REFERENCES organisation_claims(organisation_id, claim_name)
				ON UPDATE CASCADE
					ON DELETE CASCADE,
    UNIQUE (organisation_id, user_id, claim_name)
);
