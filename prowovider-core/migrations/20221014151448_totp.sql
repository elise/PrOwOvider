ALTER TABLE organisation_users ADD COLUMN totp_secret TEXT;
ALTER TABLE organisation_users ADD COLUMN totp_confirmed BOOLEAN NOT NULL DEFAULT FALSE;
