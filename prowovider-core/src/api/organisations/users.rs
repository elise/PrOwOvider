/*
	Copyright 2022 Elise Amber Katze <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

pub mod claims;
pub mod scopes;
pub mod totp;

use std::borrow::Cow;

use axum::{extract, http::HeaderValue, Json};
use openid_provider::axum::BasicAuth;
use uuid::Uuid;

use crate::{
	api::{admin_auth::invalid_auth, error::WrappedApiError, ApiResult, JsonResult},
	models::{Backend, OrganisationModel, OrganisationUserModel},
	settings::Administrator,
};

use super::{validate_auth, AuthResult};

pub async fn list_users_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path(uuid): extract::Path<Uuid>,
) -> JsonResult<Vec<prowovider_api::User>> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;
	Ok(Json(
		backend
			.get_users_by_organisation_id(uuid)
			.await?
			.into_iter()
			.map(prowovider_api::User::from)
			.collect(),
	))
}

pub async fn resolve_username(
	administrator: Option<Administrator>,
	user_auth: Option<BasicAuth>,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, username)): extract::Path<(Uuid, String)>,
) -> JsonResult<Uuid> {
	if let Some(administrator) = administrator {
		if !administrator.organisations.is_empty()
			&& !administrator.organisations.contains(&organisation_id)
		{
			return Err(WrappedApiError::invalid_auth(
				Some(Cow::Borrowed("Invalid admin credentials")),
				HeaderValue::from_static(r#"Basic charset="UTF-8""#),
			));
		}
	} else if let Some(user_auth) = user_auth {
		backend.validate_organisation_exists(organisation_id).await?;

		match backend
			.verify_credentials_and_fetch_user(
				organisation_id,
				user_auth.0.as_str(),
				user_auth.1.as_deref().unwrap_or_default(),
			)
			.await?
		{
			Some(_) => {}
			_ => {
				return Err(WrappedApiError::invalid_auth(
					Some(Cow::Borrowed("Invalid user credentials")),
					HeaderValue::from_static(r#"Basic charset="UTF-8""#),
				))
			}
		}
	} else {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;

	let user_id = match backend.get_user_id_by_username(organisation_id, username.as_str()).await? {
		Some(user_id) => user_id,
		None => return Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("User not found")))),
	};

	Ok(Json(user_id))
}

pub async fn create_user_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path(uuid): extract::Path<Uuid>,
	extract::Json(body): extract::Json<prowovider_api::UserPassword<'static, 'static>>,
) -> JsonResult<prowovider_api::User> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	let username = match body.username {
		Some(username) => username,
		None => {
			return Err(WrappedApiError::missing_parameter(Some(Cow::Borrowed(
				"Missing 'username' parameter",
			))))
		}
	};

	let password = match body.password {
		Some(password) => password,
		None => {
			return Err(WrappedApiError::missing_parameter(Some(Cow::Borrowed(
				"Missing 'password' parameter",
			))))
		}
	};

	backend.validate_organisation_exists(uuid).await?;
	if backend.user_exists_by_username(uuid, username.as_ref()).await? {
		return Err(WrappedApiError::item_already_exists(Some(Cow::Borrowed("Username is taken"))));
	}

	let res = Json(backend.new_user(uuid, username.as_ref(), password.as_ref()).await?.into());

	Ok(res)
}

pub async fn get_user_on_organisation(
	administrator: Option<Administrator>,
	user_auth: Option<BasicAuth>,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id)): extract::Path<(Uuid, Uuid)>,
) -> JsonResult<prowovider_api::User> {
	validate_auth(&backend, administrator, user_auth, organisation_id, user_id).await?;

	backend.validate_organisation_exists(organisation_id).await?;

	match backend.get_user_by_user_id(organisation_id, user_id).await? {
		Some(user) => Ok(Json(user.into())),
		None => Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("User not found")))),
	}
}

pub async fn update_username_or_password_of_user_on_organisation(
	administrator: Option<Administrator>,
	user_auth: Option<BasicAuth>,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id)): extract::Path<(Uuid, Uuid)>,
	extract::Json(body): extract::Json<prowovider_api::UserPassword<'static, 'static>>,
) -> ApiResult<()> {
	let auth_res =
		validate_auth(&backend, administrator, user_auth, organisation_id, user_id).await?;

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_user_exists(organisation_id, user_id).await?;

	match body.username.as_deref() {
		Some(username) => {
			if auth_res == AuthResult::User {
				return Err(WrappedApiError::invalid_auth(
					Some(Cow::Borrowed("Only administrators can change usernames")),
					HeaderValue::from_static(
						r#"Basic realm="Changing username requires administrator credentials", charset="UTF-8""#,
					),
				));
			}
			if backend.user_exists_by_username(organisation_id, username).await? {
				return Err(WrappedApiError::item_already_exists(Some(Cow::Borrowed(
					"Username is taken",
				))));
			}

			match body.password.as_deref() {
				Some(password) => {
					backend
						.user_update_username_password(organisation_id, user_id, username, password)
						.await?
				}
				None => backend.user_update_username(organisation_id, user_id, username).await?,
			}
		}
		None => match body.password.as_deref() {
			Some(password) => {
				backend.user_update_password(organisation_id, user_id, password).await?
			}
			None => {
				return Err(WrappedApiError::missing_parameter(Some(Cow::Borrowed(
					"Missing 'username' or 'password' parameter",
				))))
			}
		},
	}

	Ok(())
}

pub async fn delete_user_on_organisation(
	administrator: Option<Administrator>,
	user_auth: Option<BasicAuth>,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id)): extract::Path<(Uuid, Uuid)>,
) -> ApiResult<()> {
	validate_auth(&backend, administrator, user_auth, organisation_id, user_id).await?;

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_user_exists(organisation_id, user_id).await?;

	backend.delete_user(organisation_id, user_id).await?;
	Ok(())
}
