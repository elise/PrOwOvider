use std::borrow::Cow;

use axum::{extract, Json};
use openid_provider::axum::BasicAuth;
use prowovider_api::TwoFactorCodeParameter;
use totp_rs::TOTP;
use uuid::Uuid;

use crate::{
	api::{error::WrappedApiError, organisations::validate_auth, ApiResult, JsonResult},
	models::{Backend, OrganisationUserModel},
	settings::Administrator,
};

pub fn build_totp(
	organisation_id: Uuid,
	secret: &str,
	username: String,
) -> Result<TOTP<&[u8]>, totp_rs::TotpUrlError> {
	TOTP::new(
		totp_rs::Algorithm::SHA1,
		8,
		1,
		30,
		secret.as_bytes(),
		Some(organisation_id.to_string()),
		username,
	)
}

pub fn validate_totp<T: AsRef<[u8]>>(totp: &TOTP<T>, input: &str) -> Result<(), WrappedApiError> {
	match totp.generate_current()?.as_str() == input {
		true => Ok(()),
		false => Err(WrappedApiError::totp_validation_error()),
	}
}

pub async fn post_totp_on_user(
	administrator: Option<Administrator>,
	user_auth: Option<BasicAuth>,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id)): extract::Path<(Uuid, Uuid)>,
) -> JsonResult<String> {
	validate_auth(&backend, administrator, user_auth, organisation_id, user_id).await?;

	let user = backend
		.get_user_by_user_id(organisation_id, user_id)
		.await?
		.ok_or_else(|| WrappedApiError::item_not_found(Some(Cow::Borrowed("User not found"))))?;

	if user.totp_secret.is_some() {
		return Err(WrappedApiError::item_already_exists(Some(Cow::Borrowed(
			"User already has TOTP enabled",
		))));
	}

	let secret =
		base32::encode(base32::Alphabet::RFC4648 { padding: false }, &rand::random::<[u8; 16]>());

	let totp = build_totp(organisation_id, secret.as_str(), user.username.clone())?;

	backend.user_update_totp_secret(organisation_id, user_id, Some(secret.as_str())).await?;
	backend.user_update_totp_confirmed(organisation_id, user_id, false).await?;

	Ok(Json(totp.get_url()))
}

pub async fn delete_totp_on_user(
	administrator: Option<Administrator>,
	user_auth: Option<BasicAuth>,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id)): extract::Path<(Uuid, Uuid)>,
	extract::Query(query): extract::Query<TwoFactorCodeParameter>,
) -> ApiResult<()> {
	validate_auth(&backend, administrator, user_auth, organisation_id, user_id).await?;

	let user = backend
		.get_user_by_user_id(organisation_id, user_id)
		.await?
		.ok_or_else(|| WrappedApiError::item_not_found(Some(Cow::Borrowed("User not found"))))?;

	if let Some(totp_secret) = user.totp_secret.as_deref() {
		let totp = build_totp(organisation_id, totp_secret, user.username.clone())?;
		validate_totp(&totp, query.code.as_str())?;

		backend.user_update_totp_secret(organisation_id, user_id, None).await?;
	}

	if user.totp_confirmed {
		backend.user_update_totp_confirmed(organisation_id, user_id, false).await?;
	}

	Ok(())
}

pub async fn confirm_totp_on_user(
	administrator: Option<Administrator>,
	user_auth: Option<BasicAuth>,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id)): extract::Path<(Uuid, Uuid)>,
	extract::Query(query): extract::Query<TwoFactorCodeParameter>,
) -> ApiResult<()> {
	validate_auth(&backend, administrator, user_auth, organisation_id, user_id).await?;

	let user = backend
		.get_user_by_user_id(organisation_id, user_id)
		.await?
		.ok_or_else(|| WrappedApiError::item_not_found(Some(Cow::Borrowed("User not found"))))?;

	if user.totp_confirmed {
		Err(WrappedApiError::bad_request(Some(Cow::Borrowed("TOTP already enabled and confirmed"))))
	} else if let Some(totp_secret) = user.totp_secret.as_deref() {
		let totp = build_totp(organisation_id, totp_secret, user.username.clone())?;
		validate_totp(&totp, query.code.as_str())?;

		backend.user_update_totp_confirmed(organisation_id, user_id, true).await?;
		Ok(())
	} else {
		Err(WrappedApiError::bad_request(Some(Cow::Borrowed("TOTP not setup for user"))))
	}
}
