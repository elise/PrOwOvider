use std::borrow::Cow;

use axum::http::HeaderValue;
use hyper::StatusCode;
use openid_types::endpoints::requests::CodeCallback;
use prowovider_api::TwoFactorCodeParameter;
use prowovider_core::{
	api::tokens::{LoginState, Token},
	models::{
		ClientRedirectUriModel, ClientType, OrganisationClientModel, OrganisationModel,
		OrganisationScopeModel, OrganisationUserModel,
	},
};
use totp_rs::TOTP;

use crate::api::{
	path, read_settings,
	refresh_token::{authentication_request, REDIRECT_URI, STATE},
	spawn_server, ORG_1_UUID,
};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn enable_totp_login_disable_totp_on_user() {
	let settings = read_settings();
	let port = spawn_server(settings.clone(), pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	pool.new_scope(ORG_1_UUID, "openid", true).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "meow").await.unwrap();
	let client =
		pool.new_client(ORG_1_UUID, "test-client", ClientType::Confidential, None).await.unwrap();
	pool.create_redirect_uri(client.id, REDIRECT_URI).await.unwrap();

	let api_path = format!("/api/v1/organisations/{}/users/{}/totp", ORG_1_UUID, user1.id);
	let res = reqwest::Client::default()
		.post(path(api_path.as_str(), port))
		.basic_auth(user1.username.clone(), Some("meow"))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	let url: String = res.json().await.unwrap();

	let totp = TOTP::<Vec<u8>>::from_url(url.as_str()).unwrap();

	let api_path = format!("/api/v1/organisations/{}/users/{}/totp/confirm", ORG_1_UUID, user1.id);
	let mut status = None;
	for _ in 0..2 {
		let res = reqwest::Client::default()
			.post(path(api_path.as_str(), port))
			.query(&TwoFactorCodeParameter { code: totp.generate_current().unwrap() })
			.basic_auth(user1.username.clone(), Some("meow"))
			.send()
			.await
			.unwrap();
		status = Some(res.status());
		if status == Some(StatusCode::OK) {
			break;
		}
	}
	assert_eq!(status, Some(StatusCode::OK));

	assert!(pool.get_user_by_user_id(ORG_1_UUID, user1.id).await.unwrap().unwrap().totp_confirmed);

	let settings = settings.openid_connect.get(&ORG_1_UUID).unwrap().clone();
	let key = settings.jwt.by_optional_id(None).unwrap();

	let authentication_request = authentication_request(client.id);
	let login_state = Token::LoginState(Cow::Owned(LoginState::new(
		vec![String::from("openid")],
		authentication_request,
		ORG_1_UUID,
		&settings,
	)))
	.encode(key)
	.unwrap();

	let login_resp = reqwest::Client::builder()
		.redirect(reqwest::redirect::Policy::none())
		.build()
		.unwrap()
		.post(path(format!("/api/v1/openid/{}/login", ORG_1_UUID).as_str(), port))
		.form(&prowovider_api::LoginRequest {
			login_state: login_state.clone(),
			username: user1.username.clone(),
			password: String::from("meow"),
			totp_code: None,
		})
		.send()
		.await
		.unwrap();
	assert_eq!(
		login_resp.status(),
		StatusCode::OK,
		"JSON: {}",
		serde_json::to_string_pretty(&login_resp.json::<serde_json::Value>().await.unwrap())
			.unwrap()
	);

	let mut status = None;
	for _ in 0..2 {
		let login_resp = reqwest::Client::builder()
			.redirect(reqwest::redirect::Policy::none())
			.build()
			.unwrap()
			.post(path(format!("/api/v1/openid/{}/login", ORG_1_UUID).as_str(), port))
			.form(&prowovider_api::LoginRequest {
				login_state: login_state.clone(),
				username: user1.username.clone(),
				password: String::from("meow"),
				totp_code: Some(totp.generate_current().unwrap()),
			})
			.send()
			.await
			.unwrap();
		status = Some(login_resp.status());
		if status == Some(StatusCode::FOUND) {
			let redirect_uri = login_resp
				.headers()
				.get(axum::http::header::LOCATION)
				.map(HeaderValue::to_str)
				.unwrap()
				.unwrap();
			let parsed_redirect_uri = url::Url::parse(redirect_uri).unwrap();
			let CodeCallback { state, .. } =
				serde_urlencoded::from_str(parsed_redirect_uri.query().unwrap()).unwrap();
			assert_eq!(state.as_deref(), Some(STATE));
			break;
		}
	}
	assert_eq!(status, Some(StatusCode::FOUND));

	let api_path = format!("/api/v1/organisations/{}/users/{}/totp", ORG_1_UUID, user1.id);
	let mut status = None;
	for _ in 0..2 {
		let res = reqwest::Client::default()
			.delete(path(api_path.as_str(), port))
			.query(&TwoFactorCodeParameter { code: totp.generate_current().unwrap() })
			.basic_auth(user1.username.as_str(), Some("meow"))
			.send()
			.await
			.unwrap();
		status = Some(res.status());
		if res.status().is_success() {
			break;
		}
	}
	assert_eq!(status, Some(StatusCode::OK));

	assert!(!pool.get_user_by_user_id(ORG_1_UUID, user1.id).await.unwrap().unwrap().totp_confirmed);
}
