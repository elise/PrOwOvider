use std::borrow::Cow;

use axum::http::HeaderValue;
use hyper::StatusCode;
use jsonwebtoken::Validation;
use openid_types::endpoints::{
	requests::{CodeCallback, CodeTokenRequest, CodeTokenRequestExtras},
	responses::TokenResponse,
};
use prowovider_core::{
	api::tokens::{LoginState, Token},
	models::{
		ClaimType, ClientRedirectUriModel, ClientType, OrganisationClaimDestinationModel,
		OrganisationClaimModel, OrganisationClientModel, OrganisationModel,
		OrganisationScopeClaimMapModel, OrganisationScopeModel, OrganisationUserModel,
	},
};
use serde_json::Value;

use crate::api::{
	global_test_administrator, path, read_settings,
	refresh_token::{authentication_request, REDIRECT_URI, STATE},
	spawn_server, ORG_1_UUID,
};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_default_claim_value() {
	let settings = read_settings();
	let port = spawn_server(settings.clone(), pool.clone()).await;

	let admin = global_test_administrator();

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let _claim1 = pool
		.new_claim(ORG_1_UUID, "claim-1", None, &Value::String(String::from("meow")))
		.await
		.unwrap();

	let api_path = format!("/api/v1/organisations/{}/claims/claim-1/default", ORG_1_UUID);
	let res = reqwest::Client::default()
		.get(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);
	let json: String = res.json().await.unwrap();
	assert_eq!(json.as_str(), "meow");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn set_default_claim_value() {
	let settings = read_settings();
	let port = spawn_server(settings.clone(), pool.clone()).await;

	let admin = global_test_administrator();

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let _claim1 =
		pool.new_claim(ORG_1_UUID, "claim-1", None, &serde_json::Value::Null).await.unwrap();

	let api_path = format!("/api/v1/organisations/{}/claims/claim-1/default", ORG_1_UUID);
	let res = reqwest::Client::default()
		.put(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.json(&"meow")
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	let default_value = pool
		.get_default_value_by_organisation_id_and_claim_name(ORG_1_UUID, "claim-1")
		.await
		.unwrap();

	assert_eq!(default_value, Some(serde_json::Value::String(String::from("meow"))));
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn set_default_claim_value_badly_typed() {
	let settings = read_settings();
	let port = spawn_server(settings.clone(), pool.clone()).await;

	let admin = global_test_administrator();

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let _claim1 =
		pool.new_claim(ORG_1_UUID, "claim-1", Some(ClaimType::Number), &Value::Null).await.unwrap();

	let api_path = format!("/api/v1/organisations/{}/claims/claim-1/default", ORG_1_UUID);
	let res = reqwest::Client::default()
		.put(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.json(&"meow")
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::BAD_REQUEST);

	let default_value = pool
		.get_default_value_by_organisation_id_and_claim_name(ORG_1_UUID, "claim-1")
		.await
		.unwrap();

	assert_eq!(default_value, Some(serde_json::Value::Null));
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn use_default_claim_value() {
	let settings = read_settings();
	let port = spawn_server(settings.clone(), pool.clone()).await;

	let _admin = global_test_administrator();

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	pool.new_scope(ORG_1_UUID, "openid", true).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "meow").await.unwrap();
	let client =
		pool.new_client(ORG_1_UUID, "test-client", ClientType::Confidential, None).await.unwrap();
	pool.create_redirect_uri(client.id, REDIRECT_URI).await.unwrap();

	let claim1 = pool
		.new_claim(ORG_1_UUID, "claim-1", None, &Value::String(String::from("meow")))
		.await
		.unwrap();

	pool.create_scope_claim_mapping(ORG_1_UUID, "openid", claim1.claim_name.as_str())
		.await
		.unwrap();

	pool.create_destination(
		ORG_1_UUID,
		claim1.claim_name.as_str(),
		prowovider_core::models::ClaimDestination::IdToken,
	)
	.await
	.unwrap();
	pool.create_destination(
		ORG_1_UUID,
		claim1.claim_name.as_str(),
		prowovider_core::models::ClaimDestination::UserInfo,
	)
	.await
	.unwrap();

	let settings = settings.openid_connect.get(&ORG_1_UUID).unwrap().clone();
	let key = settings.jwt.by_optional_id(None).unwrap();

	let authentication_request = authentication_request(client.id);
	let login_state = Token::LoginState(Cow::Owned(LoginState::new(
		vec![String::from("openid")],
		authentication_request,
		ORG_1_UUID,
		&settings,
	)))
	.encode(key)
	.unwrap();

	let login_resp = reqwest::Client::builder()
		.redirect(reqwest::redirect::Policy::none())
		.build()
		.unwrap()
		.post(path(format!("/api/v1/openid/{}/login", ORG_1_UUID).as_str(), port))
		.form(&prowovider_api::LoginRequest {
			login_state: login_state.clone(),
			username: user1.username.clone(),
			password: String::from("meow"),
			totp_code: None,
		})
		.send()
		.await
		.unwrap();
	assert_eq!(
		login_resp.status(),
		StatusCode::FOUND,
		"JSON: {}",
		serde_json::to_string_pretty(&login_resp.json::<serde_json::Value>().await.unwrap())
			.unwrap()
	);

	let redirect_uri = login_resp
		.headers()
		.get(axum::http::header::LOCATION)
		.map(HeaderValue::to_str)
		.unwrap()
		.unwrap();
	let parsed_redirect_uri = url::Url::parse(redirect_uri).unwrap();
	let CodeCallback { code: auth_code, state } =
		serde_urlencoded::from_str(parsed_redirect_uri.query().unwrap()).unwrap();
	assert_eq!(state.as_deref(), Some(STATE));

	let token_resp = reqwest::Client::builder()
		.redirect(reqwest::redirect::Policy::none())
		.build()
		.unwrap()
		.post(path(format!("/api/v1/openid/{}/token", ORG_1_UUID).as_str(), port))
		.form(&CodeTokenRequest {
			grant_type: openid_types::endpoints::requests::GrantType::AuthorizationCode,
			extras: CodeTokenRequestExtras {
				code: auth_code.to_string(),
				redirect_uri: Some(String::from(REDIRECT_URI)),
				client_id: None,
				client_secret: None,
			},
		})
		.basic_auth(client.id.to_string(), Some(client.secret))
		.send()
		.await
		.unwrap();
	assert_eq!(
		token_resp.status(),
		StatusCode::OK,
		"JSON: {}",
		serde_json::to_string_pretty(&token_resp.json::<serde_json::Value>().await.unwrap())
			.unwrap()
	);

	let token_resp_json: TokenResponse = token_resp.json().await.unwrap();
	let token: Token = jsonwebtoken::decode(
		token_resp_json.id_token.unwrap().as_str(),
		&key.decoding,
		&Validation::new(key.algorithm),
	)
	.unwrap()
	.claims;

	let decoded_id_token = match token {
		Token::OpenIDToken(x) => x,
		e => panic!("expected Token::OpenIDToken, found {:#?}", e),
	};

	let claim = decoded_id_token.extra.get(&claim1.claim_name).and_then(Value::as_str);
	assert_eq!(claim, Some("meow"));
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn use_default_claim_value_typed() {
	let settings = read_settings();
	let port = spawn_server(settings.clone(), pool.clone()).await;

	let _admin = global_test_administrator();

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	pool.new_scope(ORG_1_UUID, "openid", true).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "meow").await.unwrap();
	let client =
		pool.new_client(ORG_1_UUID, "test-client", ClientType::Confidential, None).await.unwrap();
	pool.create_redirect_uri(client.id, REDIRECT_URI).await.unwrap();

	let claim1 = pool
		.new_claim(
			ORG_1_UUID,
			"claim-1",
			Some(ClaimType::String),
			&Value::String(String::from("meow")),
		)
		.await
		.unwrap();

	pool.create_scope_claim_mapping(ORG_1_UUID, "openid", claim1.claim_name.as_str())
		.await
		.unwrap();

	pool.create_destination(
		ORG_1_UUID,
		claim1.claim_name.as_str(),
		prowovider_core::models::ClaimDestination::IdToken,
	)
	.await
	.unwrap();
	pool.create_destination(
		ORG_1_UUID,
		claim1.claim_name.as_str(),
		prowovider_core::models::ClaimDestination::UserInfo,
	)
	.await
	.unwrap();

	let settings = settings.openid_connect.get(&ORG_1_UUID).unwrap().clone();
	let key = settings.jwt.by_optional_id(None).unwrap();

	let authentication_request = authentication_request(client.id);
	let login_state = Token::LoginState(Cow::Owned(LoginState::new(
		vec![String::from("openid")],
		authentication_request,
		ORG_1_UUID,
		&settings,
	)))
	.encode(key)
	.unwrap();

	let login_resp = reqwest::Client::builder()
		.redirect(reqwest::redirect::Policy::none())
		.build()
		.unwrap()
		.post(path(format!("/api/v1/openid/{}/login", ORG_1_UUID).as_str(), port))
		.form(&prowovider_api::LoginRequest {
			login_state: login_state.clone(),
			username: user1.username.clone(),
			password: String::from("meow"),
			totp_code: None,
		})
		.send()
		.await
		.unwrap();
	assert_eq!(
		login_resp.status(),
		StatusCode::FOUND,
		"JSON: {}",
		serde_json::to_string_pretty(&login_resp.json::<serde_json::Value>().await.unwrap())
			.unwrap()
	);

	let redirect_uri = login_resp
		.headers()
		.get(axum::http::header::LOCATION)
		.map(HeaderValue::to_str)
		.unwrap()
		.unwrap();
	let parsed_redirect_uri = url::Url::parse(redirect_uri).unwrap();
	let CodeCallback { code: auth_code, state } =
		serde_urlencoded::from_str(parsed_redirect_uri.query().unwrap()).unwrap();
	assert_eq!(state.as_deref(), Some(STATE));

	let token_resp = reqwest::Client::builder()
		.redirect(reqwest::redirect::Policy::none())
		.build()
		.unwrap()
		.post(path(format!("/api/v1/openid/{}/token", ORG_1_UUID).as_str(), port))
		.form(&CodeTokenRequest {
			grant_type: openid_types::endpoints::requests::GrantType::AuthorizationCode,
			extras: CodeTokenRequestExtras {
				code: auth_code.to_string(),
				redirect_uri: Some(String::from(REDIRECT_URI)),
				client_id: None,
				client_secret: None,
			},
		})
		.basic_auth(client.id.to_string(), Some(client.secret))
		.send()
		.await
		.unwrap();
	assert_eq!(
		token_resp.status(),
		StatusCode::OK,
		"JSON: {}",
		serde_json::to_string_pretty(&token_resp.json::<serde_json::Value>().await.unwrap())
			.unwrap()
	);

	let token_resp_json: TokenResponse = token_resp.json().await.unwrap();
	let token: Token = jsonwebtoken::decode(
		token_resp_json.id_token.unwrap().as_str(),
		&key.decoding,
		&Validation::new(key.algorithm),
	)
	.unwrap()
	.claims;

	let decoded_id_token = match token {
		Token::OpenIDToken(x) => x,
		e => panic!("expected Token::OpenIDToken, found {:#?}", e),
	};

	let claim = decoded_id_token.extra.get(&claim1.claim_name).and_then(Value::as_str);
	assert_eq!(claim, Some("meow"));
}
